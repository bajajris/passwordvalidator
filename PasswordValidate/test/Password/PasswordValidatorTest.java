package Password;

import static org.junit.Assert.*;

import org.junit.Test;

/*
 * Author - Rishabh Bajaj 
*/
public class PasswordValidatorTest {
	@Test
	public void testHasValidCaseCharsRegular() {
		boolean result = PasswordValidator.hasValidCaseChars("AAl");
		assertTrue("Invalid case chars", result);
	}
	@Test
	public void testHasValidCaseCharsExceptionBlank() {
		boolean result = PasswordValidator.hasValidCaseChars("");
		assertFalse("Invalid case chars", result);
	}
	@Test
	public void testHasValidCaseCharsExceptionNull() {
		boolean result = PasswordValidator.hasValidCaseChars(null);
		assertFalse("Invalid case chars", result);
	}

	@Test
	public void testHasValidCaseCharsBoundryIn() {
		boolean result = PasswordValidator.hasValidCaseChars("aA");
		assertTrue("Invalid case chars", result);
	}
	@Test
	public void testHasValidCaseCharsExceptionNumbers() {
		boolean result = PasswordValidator.hasValidCaseChars("89999");
		assertFalse("Invalid case chars", result);
	}
	
	@Test
	public void testHasValidCaseCharsBoundryOutUpper() {
		boolean result = PasswordValidator.hasValidCaseChars("AAAAA");
		assertFalse("Invalid case chars", result);
	}
	
	@Test
	public void testHasValidCaseCharsBoundryOutLower() {
		boolean result = PasswordValidator.hasValidCaseChars("aaaaa");
		assertFalse("Invalid case chars", result);
	}

	@Test
	public void testCheckValidPasswordLengthRegular() {
		assertTrue("Invalid Password Length", PasswordValidator.checkValidPasswordLength("abcde1234567"));
	}

	@Test
	public void testCheckValidPasswordLengthException() {
		assertFalse("Invalid Password Length", PasswordValidator.checkValidPasswordLength(null));
	}

	@Test
	public void testCheckValidPasswordLengthExceptionSpaces() {
		assertFalse("Invalid Password Length", PasswordValidator.checkValidPasswordLength("               "));
	}

	@Test
	public void testCheckValidPasswordLengthBoundryIn() {
		assertTrue("Invalid Password Length", PasswordValidator.checkValidPasswordLength("12345678"));
	}

	@Test
	public void testCheckValidPasswordLengthBoundryOut() {
		assertFalse("Invalid Password Length", PasswordValidator.checkValidPasswordLength("1234567"));
	}
	
	@Test
	public void testHasTwoDigitsRegular() {
		assertTrue("Does not have two digits", PasswordValidator.hasTwoDigits("1234jagsdja"));
	}
	
	@Test
	public void testHasTwoDigitsException() {
		assertFalse("Does not have two digits", PasswordValidator.hasTwoDigits(null));
	}
	
	@Test
	public void testHasTwoDigitsBoundaryIn() {
		assertTrue("Does not have two digits", PasswordValidator.hasTwoDigits("ABCD34EFGHI"));

	}

	@Test
	public void testHasTwoDigitsBoundaryOut() {
		assertFalse("Does not have two digits", PasswordValidator.hasTwoDigits("ABCD3EFGHI"));
	}
	
	
	
}











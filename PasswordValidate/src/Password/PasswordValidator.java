package Password;

/**
 * 
 * @author Rishabh
 *
 */
public class PasswordValidator {
	private static int MIN_LENGTH = 8;

	public static boolean checkValidPasswordLength(String password) {
		if (password != null) {
			return password.trim().length() >= MIN_LENGTH;
		}

		return false;
	}

	public static boolean hasTwoDigits(String password) {
		if(password!=null) {
			return password.matches(".*[0-9].*[0-9].*");
		}
		
		return false;
	}
	
	public static boolean hasValidCaseChars(String password) {
		return password!=null && password.matches(".*[A-Z]+.*")&&password.matches(".*[a-z]+.*");
	}
}
